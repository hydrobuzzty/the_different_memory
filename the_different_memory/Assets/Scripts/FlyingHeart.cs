﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingHeart : MonoBehaviour
{
    public Transform targetA;
    public Transform targetB;

    public AnimationCurve lerpCurve;
    public Vector2 lerpOffset;

    public float lerpTime = 3f;

    private float _timer = 0f;

    void Update()
    {
        _timer += Time.deltaTime;

        if (_timer > lerpTime)
        {
            _timer = lerpTime;
        }

        float lerpRatio = _timer / lerpTime;

        Vector2 positionOffset = lerpCurve.Evaluate(lerpRatio) * lerpOffset;

        transform.position = Vector2.Lerp(targetA.position, targetB.position, lerpRatio) + positionOffset;
    }


}
