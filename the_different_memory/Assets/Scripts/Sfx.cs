﻿using System;
using System.Collections;
using System.Collections.Generic;
using Memory;
using UnityEngine;

public class Sfx : MonoBehaviour
{
    public AudioSource source;
    public AudioClip cardFlip;
    public AudioClip matchFound;
    public AudioClip noMatchFound;
    public AudioClip barClosing;

    private void Start()
    {
        GameEvents.current.CardFlipSound += PlayFlipSound;
        GameEvents.current.MatchFoundSound += PlayMatchFoundSound;
        GameEvents.current.NoMatchFoundSound += PlayNoMatchFoundSound;
        GameEvents.current.BarClosingSound += PlayBarClosingSound;
    }

    private void PlayFlipSound()
    {
        source.PlayOneShot(cardFlip);
    }

    private void PlayMatchFoundSound()
    {
        source.PlayOneShot(matchFound);
    }

    private void PlayNoMatchFoundSound()
    {
        source.PlayOneShot(noMatchFound);
    }

    private void PlayBarClosingSound()
    {
        source.PlayOneShot(barClosing);
    }
}