﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DOTweenController : MonoBehaviour
{
    [SerializeField]
    private Vector2 targetLocation = Vector2.zero;
    
    [SerializeField, Range (1.0f, 10.0f)]
    private float moveDuration = 1.0f;

    [SerializeField] 
    private Ease moveEase = Ease.Linear;

    [SerializeField]
    private DoTweenType doTweenType = DoTweenType.MovementOneWay;
    
    private enum DoTweenType
    {
        MovementOneWay
    }
        
        
    // Start is called before the first frame update
    void Start()
    {
        if (doTweenType == DoTweenType.MovementOneWay)
        {
            if (targetLocation == Vector2.zero)
            {
                transform.DOMove(targetLocation, moveDuration).SetEase(moveEase);
                targetLocation = transform.position;
            }
        }
    }
}
