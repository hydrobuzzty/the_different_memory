﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddCards : MonoBehaviour
{
    [SerializeField] private Transform memoryField;
    [SerializeField] private GameObject card;
    
    private void Awake()
    {
        for (int i = 0; i < 24; i++)
        {
            GameObject button = Instantiate(card);
            button.name = "" + i;
            button.transform.SetParent(memoryField, false);
        }
    }
}
