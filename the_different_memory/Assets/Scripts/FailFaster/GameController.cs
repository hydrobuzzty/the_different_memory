﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    [SerializeField] private Sprite CardBackgroundImage;
    
    public Sprite[] puzzles;
    
    public List<Sprite> gamePuzzles = new List<Sprite>();
    
    public List<Button> btns = new List<Button>();

    private bool firstGuess, secondGuess;
    
    private int countGuesses;

    private int countCorrectGuesses;
    
    private int gameGuesses;

    private string firstGuessCard, secondGuessCard;

    private int firstGuessIndex, secondGuessIndex;

    public void Awake()
    {
        puzzles = Resources.LoadAll<Sprite>("Sprites/Cards");
    }

    private void Start()
    {
        GetCards();
        AddListeners();
        AddGamePuzzles();
    }

    public void GetCards()
    {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Card");

        for (int i = 0; i < objects.Length; i++)
        {
            btns.Add(objects[i].GetComponent<Button>());
            btns[i].image.sprite = CardBackgroundImage;
        }
    }

    public void AddGamePuzzles()
    {
        int looper = btns.Count;
        int index = 0;

        for (int i = 0; i < looper; i++)
        {
            if (index == looper/2)
            {
                index = 0;
            }
            
            gamePuzzles.Add(puzzles[index]);

            index++;
        }
    }
    void AddListeners()
    {
        foreach (var card in btns)
        {
            card.onClick.AddListener(PickACard);
        }
    }

    public void PickACard()
    {
        string name = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name;

        if (!firstGuess)
        {
            firstGuess = true;

            firstGuessIndex = int.Parse(UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name);

            firstGuessCard = puzzles[firstGuessIndex].name;

            btns[firstGuessIndex].image.sprite = puzzles[firstGuessIndex];
        }
        else if (!secondGuess)
        {
            secondGuess = true;

            secondGuessIndex = int.Parse(UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name);

            secondGuessCard = puzzles[secondGuessIndex].name;

            btns[secondGuessIndex].image.sprite = puzzles[secondGuessIndex];

            if (firstGuessCard == secondGuessCard)
            {
                Debug.Log("Card match!");
            }
            else
            {
                Debug.Log("Cards don't match!");
            }
        }
    }
}
