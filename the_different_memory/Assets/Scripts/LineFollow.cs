﻿using System;
using System.Collections;
using System.Collections.Generic;
using Memory;
using UnityEngine;

public class LineFollow : MonoBehaviour
{
    [SerializeField] 
    private Transform[] routes;

    private int routeToGo;

    private float tParam;

    private Vector2 heartPosition;

    private float speedModifier;

    void Start()
    {
        routeToGo = 0;
        tParam = 0f;
        speedModifier = 0.005f;
    }

    private void Update()
    {
        if (this.gameObject.activeSelf == true)
        {
            StartCoroutine(GoByTheRoute(routeToGo));
        }
        else
        {
            StopCoroutine(GoByTheRoute(routeToGo));
        }
    }

    private IEnumerator GoByTheRoute(int routeNumber)
    {
        Vector2 p0 = routes[routeNumber].GetChild(0).position;
        Vector2 p1 = routes[routeNumber].GetChild(1).position;
        Vector2 p2 = routes[routeNumber].GetChild(2).position;
        Vector2 p3 = routes[routeNumber].GetChild(3).position;

        while (tParam < 1)
        {
            tParam += Time.deltaTime * speedModifier;

            heartPosition = Mathf.Pow(1 - tParam, 3) * p0 +
                            3 * Mathf.Pow(1 - tParam, 2) * tParam * p1 +
                            3 * (1 - tParam) * Mathf.Pow(tParam, 2) * p2 +
                            Mathf.Pow(tParam, 3) * p3;

            transform.position = heartPosition;
            yield return new WaitForEndOfFrame();
        }

        tParam = 0f;

        routeToGo += 1;

        if (routeToGo > routes.Length - 1)
        {
            routeToGo = 0;
        }
        this.transform.root.gameObject.SetActive(false);
    }
}
