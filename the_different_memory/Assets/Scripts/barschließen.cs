﻿using System.Collections;
using System.Collections.Generic;
using Memory;
using Unity.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BarSchließen : MonoBehaviour
{
    [SerializeField] 
    private GameObject closed;

    public void BarClosed()
    {
        if (SceneManager.GetActiveScene().name == "Memory_LvL01")
        {
            GameEvents.current.SaveHighScoreLvL1();
        }
        if (SceneManager.GetActiveScene().name == "Memory_LvL02")
        {
            GameEvents.current.SaveHighScoreLvL2();
        }
        if (SceneManager.GetActiveScene().name == "Memory_LvL03")
        {
            GameEvents.current.SaveHighScoreLvL3();
        }
        GameEvents.current.SoundBarClosing();
        closed.SetActive(true);
        StartCoroutine(ClosedCoroutine());
    }
    
    public void BarLv03Closed()
    {
        GameEvents.current.SoundBarClosing();
        closed.SetActive(true);
        StartCoroutine(ClosedLv03Coroutine());
    }
    
    IEnumerator ClosedCoroutine()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("Hub");
    }  
    
    IEnumerator ClosedLv03Coroutine()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("Ending");
    }  
}
