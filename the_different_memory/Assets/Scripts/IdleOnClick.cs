﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleOnClick : MonoBehaviour
{

    public GameObject idle;
    private Vector3 mousePosition;
    public float moveSpeed = 1000000000f;


    // Update is called once per frame
    void Update()
    {
        mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        idle.transform.position = Vector2.Lerp(idle.transform.position, mousePosition, moveSpeed);
        
    }

    private void OnMouseDown()
    {
        idle.SetActive(true);
        StartCoroutine(DustCoroutine());
    }
    
    IEnumerator DustCoroutine()
    {
        yield return new WaitForSeconds(0.85f);
        idle.SetActive(false);
    }
}