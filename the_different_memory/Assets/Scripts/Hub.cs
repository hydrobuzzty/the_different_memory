﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Hub : MonoBehaviour
{
    public void LoadLvl01()
    {
        StartCoroutine(LoadLV01Coroutine());
    }
    
    public void LoadLvl02()
    {
        StartCoroutine(LoadLV02Coroutine());
    }
    
    public void LoadLvl03()
    {
        StartCoroutine(LoadLV03Coroutine());
    }
    
    public void BackMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    
    IEnumerator LoadLV01Coroutine()
    {
        yield return new WaitForSeconds(0.3f);
        SceneManager.LoadScene("Level01Intro");
    }
    
    IEnumerator LoadLV02Coroutine()
    {
        yield return new WaitForSeconds(0.3f);
        SceneManager.LoadScene("Level02Intro");
    }
    
    IEnumerator LoadLV03Coroutine()
    {
        yield return new WaitForSeconds(0.3f);
        SceneManager.LoadScene("Level03Intro");
    }
}
