﻿using System;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Memory
{
    public class RevealSideCards : MonoBehaviour
    {
        // References to the ScriptableObject-Card and the components of the card itself
        public GameManager manager;

        public GameObject firstRevealedCard;
        public GameObject secRevealedCard;
        public GameObject thirdRevealedCard;
        
        
        public GameObject[] bubblesOne;
        public GameObject[] bubblesTwo;
        public GameObject[] bubblesThree;

        public GameObject firstBubble;
        public GameObject secBubble;
        public GameObject thirdBubble;

        // At start set every text and sprite variable to the one set in the ScriptableObject

        private void Start()
        {
            GameEvents.current.FirstCardRevealed += SetFirstActive;
            GameEvents.current.SecondCardRevealed += SetSecondActive;
            GameEvents.current.ThirdCardRevealed += SetThirdActive;
            GameEvents.current.BubbleOneEnable += RandomBubbleOne;
            GameEvents.current.BubbleTwoEnable += RandomBubbleTwo;
            GameEvents.current.BubbleThreeEnable += RandomBubbleThree;
            GameEvents.current.DisableBubbles += DisableBubbles;
        }

        private void Update()
        {
            if (manager._firstRevealed == null)
            {
                firstRevealedCard.SetActive(false);
            }

            if (manager._secondRevealed == null)
            {
                secRevealedCard.SetActive(false);
            }

            if (manager._thirdRevealed == null)
            {
                thirdRevealedCard.SetActive(false);
            }
        }
        
        private void RandomBubbleOne()
        {
            int i = Random.Range(0, bubblesOne.Length);
            bubblesOne[i].SetActive(true);
            firstBubble = bubblesOne[i];
        }

        private void RandomBubbleTwo()
        {
            int i = Random.Range(0, bubblesTwo.Length);
            bubblesTwo[i].SetActive(true);
            secBubble = bubblesTwo[i];
        }

        private void RandomBubbleThree()
        {
            int i = Random.Range(0, bubblesThree.Length);
            bubblesThree[i].SetActive(true);
            thirdBubble = bubblesThree[i];
        }

        private void DisableBubbles()
        {
            firstBubble.SetActive(false);
            secBubble.SetActive(false);
            thirdBubble.SetActive(false);
        }

        private void SetFirstActive()
        {
            if (manager._firstRevealed != null)
            {
                firstRevealedCard.SetActive(true);
                firstRevealedCard.GetComponent<RevealedCardDisplay>().nameText.text = manager._firstRevealed.nameText.text;
                firstRevealedCard.GetComponent<RevealedCardDisplay>().likesText.text = manager._firstRevealed.likesText.text;
                firstRevealedCard.GetComponent<RevealedCardDisplay>().sexualityText.text = manager._firstRevealed.sexualityText.text;
                firstRevealedCard.GetComponent<RevealedCardDisplay>().genderText.text = manager._firstRevealed.genderText.text;
            }
        }

        private void SetSecondActive()
        {
            if (manager._secondRevealed != null)
            {
                secRevealedCard.SetActive(true);
                secRevealedCard.GetComponent<RevealedCardDisplay>().nameText.text = manager._secondRevealed.nameText.text;
                secRevealedCard.GetComponent<RevealedCardDisplay>().likesText.text = manager._secondRevealed.likesText.text;
                secRevealedCard.GetComponent<RevealedCardDisplay>().sexualityText.text = manager._secondRevealed.sexualityText.text;
                secRevealedCard.GetComponent<RevealedCardDisplay>().genderText.text = manager._secondRevealed.genderText.text;
            }
        }

        private void SetThirdActive()
        {
            if (manager._thirdRevealed != null)
            {
                thirdRevealedCard.SetActive(true);
                thirdRevealedCard.GetComponent<RevealedCardDisplay>().nameText.text = manager._thirdRevealed.nameText.text;
                thirdRevealedCard.GetComponent<RevealedCardDisplay>().likesText.text = manager._thirdRevealed.likesText.text;
                thirdRevealedCard.GetComponent<RevealedCardDisplay>().sexualityText.text = manager._thirdRevealed.sexualityText.text;
                thirdRevealedCard.GetComponent<RevealedCardDisplay>().genderText.text = manager._thirdRevealed.genderText.text;
            }
        }
    }
}
