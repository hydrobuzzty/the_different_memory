﻿using System;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Memory
{
    public class RevealedCardDisplay : MonoBehaviour
    {
        public TextMeshPro nameText;
        public TextMeshPro genderText;
        public TextMeshPro sexualityText;
        public TextMeshPro likesText;

        public SpriteRenderer cardImageRenderer;

        public GameObject blueLike;
        public GameObject pinkLike;


        private void Update()
        {
            if (likesText.text == "1")
            {
                pinkLike.SetActive(true);
                blueLike.SetActive(false);
            }
        
            if (likesText.text == "2")
            {
                pinkLike.SetActive(false);
                blueLike.SetActive(true);
            }
        }
    }
}
