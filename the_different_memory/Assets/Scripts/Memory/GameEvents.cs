﻿using System;
using UnityEngine;

namespace Memory
{
    public class GameEvents : MonoBehaviour
    {
        public static GameEvents current;

        void Awake()
        {
            current = this;
        }

        public event Action NoLikes;
        public void NoLikesMatch()
        {
            if (NoLikes != null)
            {
                NoLikes();
            }
        }
    
    
        public event Action FirstSec;
        public void FirstSecMatch()
        {
            if (FirstSec != null)
            {
                FirstSec();
            }
        }

        public event Action FirstThird;
        public void FirstThirdMatch()
        {
            if (FirstThird != null)
            {
                FirstThird();
            }
        }

        public event Action FirstSecThird;
        public void FirstSecThirdMatch()
        {
            if (FirstSecThird != null)
            {
                FirstThird();
            }
        }

        public event Action HighScoreUp;

        public void HighScoreUpOnClick()
        {
            if (HighScoreUp != null)
            {
                HighScoreUp();
            }
        }

        public event Action FirstCardRevealed;

        public void ManagerFirstRevealed()
        {
            if (FirstCardRevealed != null)
            {
                FirstCardRevealed();
            }
        }
        
        
        public event Action SecondCardRevealed;

        public void ManagerSecondRevealed()
        {
            if (SecondCardRevealed != null)
            {
                SecondCardRevealed();
            }
        }
        
        public event Action ThirdCardRevealed;

        public void ManagerThirdRevealed()
        {
            if (ThirdCardRevealed != null)
            {
                ThirdCardRevealed();
            }
        }

        public event Action LittleHearts;

        public void LittleHeartsUp()
        {
            if (LittleHearts != null)
            {
                LittleHearts();
            }
        }
        
        public event Action MiddleHearts;

        public void MiddleHeartsUp()
        {
            if (MiddleHearts != null)
            {
                MiddleHearts();
            }
        }
        
        public event Action LargeHearts;

        public void LargeHeartsUp()
        {
            if (LargeHearts != null)
            {
                LargeHearts();
            }
        }

        public event Action SaveScoreLvL1;

        public void SaveHighScoreLvL1()
        {
            if (SaveScoreLvL1 != null)
            {
                SaveScoreLvL1();
            }
        }
        
        public event Action SaveScoreLvL2;

        public void SaveHighScoreLvL2()
        {
            if (SaveScoreLvL2 != null)
            {
                SaveScoreLvL2();
            }
        }
        
        public event Action SaveScoreLvL3;

        public void SaveHighScoreLvL3()
        {
            if (SaveScoreLvL3 != null)
            {
                SaveScoreLvL3();
            }
        }

        public event Action CardFlipSound;

        public void SoundOnFlip()
        {
            if (CardFlipSound != null)
            {
                CardFlipSound();
            }
        }

        public event Action MatchFoundSound;

        public void SoundOnMatch()
        {
            if (MatchFoundSound != null)
            {
                MatchFoundSound();
            }
        }

        public event Action NoMatchFoundSound;

        public void SoundOnNoMatch()
        {
            if (NoMatchFoundSound != null)
            {
                NoMatchFoundSound();
            }
        }

        public event Action BarClosingSound;

        public void SoundBarClosing()
        {
            if (BarClosingSound != null)
            {
                BarClosingSound();
            }
        }

        public event Action BubbleOneEnable;

        public void EnableBubbleOne()
        {
            if (BubbleOneEnable != null)
            {
                BubbleOneEnable();
            }
        }
        
        public event Action BubbleTwoEnable;

        public void EnableBubbleTwo()
        {
            if (BubbleTwoEnable != null)
            {
                BubbleTwoEnable();
            }
        }
        
        
        public event Action BubbleThreeEnable;

        public void EnableBubbleThree()
        {
            if (BubbleThreeEnable != null)
            {
                BubbleThreeEnable();
            }
        }

        public event Action DisableBubbles;

        public void BubblesDisabled()
        {
            if (DisableBubbles != null)
            {
                DisableBubbles();
            }
        }
    }
}
