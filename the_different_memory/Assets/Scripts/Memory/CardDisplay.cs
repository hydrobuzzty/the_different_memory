﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Memory;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

// Script do declare what should be shown on the cards
public class CardDisplay : MonoBehaviour
{
    // References to the ScriptableObject-Card and the components of the card itself
    public Card card;

    public TextMeshPro nameText;
    public TextMeshPro genderText;
    public TextMeshPro sexualityText;
    public TextMeshPro likesText;
    public TextMeshPro dislikesText;

    public SpriteRenderer CardImageRenderer;

    // At start set every text and sprite variable to the one set in the ScriptableObject
    void Start()
    {
        nameText.text = card.name;
        genderText.text = card.gender;
        sexualityText.text = card.sexuality;
        CardImageRenderer.sprite = card.image;
        likesText.text = card.likes;
    }
    
    
    
    //------------------------------------------------------------------------------------------------------------------
    
    
    // References to the card back and the GameManager
    [SerializeField] private GameObject card_face;
    [SerializeField] private GameManager manager;
    
    // Function to deactivate the card back and set the card as revealed (implemented from GameManager-Script)
    public void OnMouseDown()
    {
        if (card_face.activeSelf && manager.CanReveal)
        {
            this.transform.DORotate(new Vector3(0, 180, 0), 0.3f);
            manager.CardRevealed(this);
            GameEvents.current.SoundOnFlip();
        }
    }
    
    // Function to unreveal the card (sets card back to active)
    public void Unreveal()
    {
        this.transform.DORotate(new Vector3(0, 0, 0), 0.3f);
        GameEvents.current.SoundOnFlip();
    }

}