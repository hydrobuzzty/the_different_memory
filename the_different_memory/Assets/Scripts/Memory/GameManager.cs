﻿using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace Memory
{
    public class GameManager : MonoBehaviour
    {
        // Variables, references, bools etc...
        public GameObject card;
        public List<Vector3> transformList;
        public List<GameObject> cardList;
        public int masterTransform;
        public bool matchFound = false;
        private int moves;
        public CardDisplay _firstRevealed;
        public CardDisplay _secondRevealed;
        public CardDisplay _thirdRevealed;
        public CardDisplay firstHighScore;
        public CardDisplay secHighScore;
        public CardDisplay thirdHighScore;
        public CardDisplay template;
        public GameObject littleHeart;
        public GameObject middleHeart;
        public GameObject largeHeart;
        public int _score = 0;
        public bool revealingStopped = false;
        public bool matchFoundHighScore = false;
        public bool firstRevealed = false;
        public bool secRevealed = false;
        public bool thirdRevealed = false;
        public bool canRevealThird = false;
        public bool keepChecking = true;
        public bool pressingAllowed = false;
        public bool shaking = true;
        public bool doublePoly = false;
        [SerializeField] private TextMeshProUGUI movesLabel;
        [SerializeField] private TextMeshProUGUI scoreLabel;
        public bool glow = false;

        // Awake takes all GameObjects with Tag "Card" and places them in a new list. Additionally the SetTransform method is called.
        private void Awake()
        {
            cardList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Card"));

            transformList = new List<Vector3>();

            GameObject[] objectsWithTag = GameObject.FindGameObjectsWithTag("Transform");

            foreach (GameObject GO in objectsWithTag)
            {
                transformList.Add(GO.transform.position);
            }

            SetTransform();
            
            GameEvents.current.LittleHearts += LittleHeartsUp;
            GameEvents.current.MiddleHearts += MiddleHeartsUp;
            GameEvents.current.LargeHearts += LargeHeartsUp;
        }
    
    
        // Start sets the amount of moves the player has to be specific for every level.
        private void Start()
        {
            Scene currentScene = SceneManager.GetActiveScene();

            string sceneName = currentScene.name;

            if (sceneName == "Memory_LvL01")
            {
                moves = 20;
                movesLabel.text = "Moves: " + moves;
            }
            else if (sceneName == "Memory_LvL02")
            {
                moves = 20;
                movesLabel.text = "Moves: " + moves;
            }
            else if (sceneName == "Memory_LvL03")
            {
                moves = 25;
                movesLabel.text = "Moves: " + moves;
            }
            
            
        }
    
    
        // Update checks if the player has got moves left, if not, Main Menu is loaded. Additionally Update calls the CheckMatch method.
        private void Update()
        {
            if (moves <= 0)
            {
                revealingStopped = true;
            }

            CheckMatch();
        
            MatchHighScore();

            if (revealingStopped && matchFound)
            {
                glow = true;
            }
            else
            {
                glow = false;
            }

            if (glow)
            {
                _firstRevealed.gameObject.transform.GetChild(7).gameObject.SetActive(true);
                _secondRevealed.gameObject.transform.GetChild(7).gameObject.SetActive(true);
                if (_thirdRevealed != null)
                {
                    _thirdRevealed.gameObject.transform.GetChild(7).gameObject.SetActive(true);
                }
            }
            else
            {
                _firstRevealed.gameObject.transform.GetChild(7).gameObject.SetActive(false);
                _secondRevealed.gameObject.transform.GetChild(7).gameObject.SetActive(false);
                if (_thirdRevealed != null)
                {
                    _thirdRevealed.gameObject.transform.GetChild(7).gameObject.SetActive(false);
                }
            }
        }
    
    
        // SetTransform sets the transform.position of all cards in the CardsList to a random transform.position out of the TransformList.
        public void SetTransform()
        {
            for (int i = 0; i < cardList.Count; i++)
            {
                masterTransform = Random.Range(0, transformList.Count);

                Vector3 cardTransform = transformList[masterTransform];

                GameObject selectedCard = cardList[i];

                card = selectedCard;

                card.transform.position = cardTransform;

                transformList.Remove(cardTransform);
            }
        }
    
    
        // CardRevealed sets _firstRevealed, _secondRevealed and _thirdRevealed to the revealed card, based on the CardDisplay script and checks, if more cards can be revealed.
        public void CardRevealed(CardDisplay card)
        {
            if (_firstRevealed == null && !revealingStopped)
            {
                _firstRevealed = card;
                firstHighScore = card;
                firstRevealed = true;
                GameEvents.current.EnableBubbleOne();
                GameEvents.current.ManagerFirstRevealed();
            }
            else if (_firstRevealed != null && _secondRevealed == null && !revealingStopped)
            {
                _secondRevealed = card;
                secHighScore = card;
                secRevealed = true;
                GameEvents.current.EnableBubbleTwo();
                GameEvents.current.ManagerSecondRevealed();
            }
            else if (_firstRevealed != null && _secondRevealed != null && !revealingStopped && !matchFound && secRevealed)
            {
                _thirdRevealed = card;
                thirdHighScore = card;
                thirdRevealed = true;
                revealingStopped = true;
                GameEvents.current.EnableBubbleThree();
                GameEvents.current.ManagerThirdRevealed();
            }
        }
    
    
        // Sets the canReveal bool to true, if another bool is true (revealingStopped tells the Game, if more cards can be revealed or not). This bool is used by the CardDisplay script.
        public bool CanReveal
        {
            get { return !revealingStopped; }
        }


        // CheckMatch checks the revealed cards gender and sexuality and sets the matchFound bool to true, whenever cards match together. If so, revealing is stopped aswell.
        private void CheckMatch()
        {
            if (_firstRevealed.genderText.text == "male")
            {
                if (_firstRevealed.sexualityText.text == "hetero")
                {
                    if (_secondRevealed.genderText.text == "female")
                    {
                        if (_secondRevealed.sexualityText.text == "homo" || _secondRevealed.sexualityText.text == "poly bi")
                        {
                            if (shaking)
                            {
                                GameEvents.current.SoundOnNoMatch();
                                _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                shaking = false;
                            }
                            matchFound = false;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                        else
                        {
                            matchFound = true;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                    }
                    else
                    {
                        if (shaking)
                        {
                            GameEvents.current.SoundOnNoMatch();
                            _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                            _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                            shaking = false;
                        }
                        matchFound = false;
                        keepChecking = false;
                        pressingAllowed = true;
                        revealingStopped = true;
                    }
                }
                else if (_firstRevealed.sexualityText.text == "homo")
                {
                    if (_secondRevealed.genderText.text == "male")
                    {
                        if (_secondRevealed.sexualityText.text == "hetero" || _secondRevealed.sexualityText.text == "poly bi")
                        {
                            if (shaking)
                            {
                                GameEvents.current.SoundOnNoMatch();
                                _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                shaking = false;
                            }
                            matchFound = false;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                        else
                        {
                            matchFound = true;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                    }
                    else
                    {
                        if (shaking)
                        {
                            GameEvents.current.SoundOnNoMatch();
                            _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                            _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                            shaking = false;
                        }
                        matchFound = false;
                        keepChecking = false;
                        pressingAllowed = true;
                        revealingStopped = true;
                    }
                }
                else if (_firstRevealed.sexualityText.text == "bi")
                {
                    if (_secondRevealed.genderText.text == "female")
                    {
                        if (_secondRevealed.sexualityText.text == "homo" || _secondRevealed.sexualityText.text == "poly bi")
                        {
                            if (shaking)
                            {
                                GameEvents.current.SoundOnNoMatch();
                                _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                shaking = false;
                            }
                            matchFound = false;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                        else
                        {
                            matchFound = true;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                    }
                    else
                    {
                        if (_secondRevealed.sexualityText.text == "hetero" || _secondRevealed.sexualityText.text == "poly bi")
                        {
                            if (shaking)
                            {
                                GameEvents.current.SoundOnNoMatch();
                                _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                shaking = false;
                            }
                            matchFound = false;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                        else
                        {
                            matchFound = true;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                    }
                }
                else
                {
                    if (_secondRevealed.genderText.text == "female")
                    {
                        if (_secondRevealed.sexualityText.text == "hetero")
                        {
                            if (_thirdRevealed.genderText.text == "female")
                            {
                                if (_thirdRevealed.sexualityText.text == "homo")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                            else
                            {
                                if (_thirdRevealed.sexualityText.text == "hetero")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                        }
                        else if (_secondRevealed.sexualityText.text == "homo")
                        {
                            if (shaking)
                            {
                                GameEvents.current.SoundOnNoMatch();
                                _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                shaking = false;
                            }
                            matchFound = false;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                        else if (_secondRevealed.sexualityText.text == "bi")
                        {
                            if (_thirdRevealed.genderText.text == "female")
                            {
                                if (_thirdRevealed.sexualityText.text == "homo")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                            else
                            {

                                if (_thirdRevealed.sexualityText.text == "hetero")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                        }
                        else
                        {

                            doublePoly = true;
                        
                            if (_thirdRevealed.genderText.text == "female")
                            {
                                if (_thirdRevealed.sexualityText.text == "homo")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                            else
                            {
                                if (_thirdRevealed.sexualityText.text == "hetero")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (_secondRevealed.sexualityText.text == "hetero")
                        {
                            if (shaking)
                            {
                                GameEvents.current.SoundOnNoMatch();
                                _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                shaking = false;
                            }
                            matchFound = false;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                        else if (_secondRevealed.sexualityText.text == "homo")
                        {
                            if (_thirdRevealed.genderText.text == "female")
                            {
                                if (_thirdRevealed.sexualityText.text == "homo")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                            else
                            {
                                if (_thirdRevealed.sexualityText.text == "hetero")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                        }
                        else if (_secondRevealed.sexualityText.text == "bi")
                        {
                            if (_thirdRevealed.genderText.text == "female")
                            {
                                if (_thirdRevealed.sexualityText.text == "homo")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                            else
                            {
                                if (_thirdRevealed.sexualityText.text == "hetero")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                        }
                        else
                        {
                        
                            doublePoly = true;
                        
                            if (_thirdRevealed.genderText.text == "female")
                            {
                                if (_thirdRevealed.sexualityText.text == "homo")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                            else
                            {
                                if (_thirdRevealed.sexualityText.text == "hetero")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (_firstRevealed.sexualityText.text == "hetero")
                {
                    if (_secondRevealed.genderText.text == "female")
                    {
                        if (shaking)
                        {
                            GameEvents.current.SoundOnNoMatch();
                            _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                            _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                            shaking = false;
                        }
                        matchFound = false;
                        keepChecking = false;
                        pressingAllowed = true;
                        revealingStopped = true;
                    }
                    else
                    {
                        if (_secondRevealed.sexualityText.text == "homo" ||
                            _secondRevealed.sexualityText.text == "poly bi")
                        {
                            if (shaking)
                            {
                                GameEvents.current.SoundOnNoMatch();
                                _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                shaking = false;
                            }
                            matchFound = false;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                        else
                        {
                            matchFound = true;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                    }
                }
                else if (_firstRevealed.sexualityText.text == "homo")
                {
                    if (_secondRevealed.genderText.text == "female")
                    {
                        if (_secondRevealed.sexualityText.text == "hetero" || _secondRevealed.sexualityText.text == "poly bi")
                        {                    
                            if (shaking)
                            {
                                GameEvents.current.SoundOnNoMatch();
                                _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                shaking = false;
                            }
                            matchFound = false;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                        else
                        {
                            matchFound = true;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                    }
                    else
                    {
                        if (shaking)
                        {
                            GameEvents.current.SoundOnNoMatch();
                            _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                            _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                            shaking = false;
                        }
                        matchFound = false;
                        keepChecking = false;
                        pressingAllowed = true;
                        revealingStopped = true;
                    }
                }
                else if (_firstRevealed.sexualityText.text == "bi")
                {
                    if (_secondRevealed.genderText.text == "female")
                    {
                        if (_secondRevealed.sexualityText.text == "hetero" || _secondRevealed.sexualityText.text == "poly bi")
                        {
                            if (shaking)
                            {
                                GameEvents.current.SoundOnNoMatch();
                                _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                shaking = false;
                            }
                            matchFound = false;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                        else
                        {
                            matchFound = true;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                    }
                    else
                    {
                        if (_secondRevealed.sexualityText.text == "homo" || _secondRevealed.sexualityText.text == "poly bi")
                        {
                            if (shaking)
                            {
                                GameEvents.current.SoundOnNoMatch();
                                _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                shaking = false;
                            }
                            matchFound = false;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                        else
                        {
                            matchFound = true;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                    }
                }
                else
                {
                    if (_secondRevealed.genderText.text == "female")
                    {
                        if (_secondRevealed.sexualityText.text == "hetero")
                        {
                            if (shaking)
                            {
                                GameEvents.current.SoundOnNoMatch();
                                _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                shaking = false;
                            }
                            matchFound = false;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                        else if (_secondRevealed.sexualityText.text == "homo")
                        {
                            if (_thirdRevealed.genderText.text == "female")
                            {
                                if (_thirdRevealed.sexualityText.text == "hetero")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                            else
                            {
                                if (_thirdRevealed.sexualityText.text == "homo")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                        }
                        else if (_secondRevealed.sexualityText.text == "bi")
                        {
                            if (_thirdRevealed.genderText.text == "female")
                            {
                                if (_thirdRevealed.sexualityText.text == "hetero")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                            else
                            {
                                if (_thirdRevealed.sexualityText.text == "homo")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                        }
                        else
                        {
                        
                            doublePoly = true;
                        
                            if (_thirdRevealed.genderText.text == "female")
                            {
                                if (_thirdRevealed.sexualityText.text == "hetero")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                            else
                            {
                                if (_thirdRevealed.sexualityText.text == "homo")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (_secondRevealed.sexualityText.text == "hetero")
                        {
                            if (_thirdRevealed.genderText.text == "female")
                            {
                                if (_thirdRevealed.sexualityText.text == "hetero")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                            else
                            {
                                if (_thirdRevealed.sexualityText.text == "homo")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                        }
                        else if (_secondRevealed.sexualityText.text == "homo")
                        {
                            if (shaking)
                            {
                                GameEvents.current.SoundOnNoMatch();
                                _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                shaking = false;
                            }
                            matchFound = false;
                            keepChecking = false;
                            pressingAllowed = true;
                            revealingStopped = true;
                        }
                        else if (_secondRevealed.sexualityText.text == "bi")
                        {
                            if (_thirdRevealed.genderText.text == "female")
                            {
                                if (_thirdRevealed.sexualityText.text == "hetero")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                            else
                            {
                                if (_thirdRevealed.sexualityText.text == "homo")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                        }
                        else
                        {
                        
                            doublePoly = true;
                        
                            if (_thirdRevealed.genderText.text == "female")
                            {
                                if (_thirdRevealed.sexualityText.text == "hetero")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                            else
                            {
                                if (_thirdRevealed.sexualityText.text == "homo")
                                {
                                    if (shaking)
                                    {
                                        GameEvents.current.SoundOnNoMatch();
                                        _firstRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90,false, true);
                                        _secondRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        _thirdRevealed.gameObject.transform.DOShakePosition(0.5f, 0.4f, 30, 90, false, true);
                                        shaking = false;
                                    }
                                    matchFound = false;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                                else
                                {
                                    matchFound = true;
                                    keepChecking = false;
                                    pressingAllowed = true;
                                    revealingStopped = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        
    
        // Match basically "resets" the game to the "Starting Point". It resets the revealed cards and the player can go on revealing cards again.
        public void Match()
        {
            if (matchFound)
            {
                moves --;
                movesLabel.text = "Moves: " + moves;
                revealingStopped = false;
                matchFound = false;
                matchFoundHighScore = false;
                glow = false;
                _firstRevealed = null;
                _secondRevealed = null;
                _thirdRevealed = null;
                firstRevealed = false;
                secRevealed = false;
                thirdRevealed = false;
                canRevealThird = false;
                keepChecking = true;
                pressingAllowed = false;
                doublePoly = false;
                GameEvents.current.SoundOnMatch();
                GameEvents.current.HighScoreUpOnClick();
                GameEvents.current.BubblesDisabled();
            }
        }

        private void MatchHighScore()
        {
            if (!matchFoundHighScore)
            {
                if (firstHighScore.likesText.text == "1")
                {
                    if (secHighScore.likesText.text == "1")
                    {
                        if (matchFound)
                        {
                            GameEvents.current.FirstSecMatch();
                            matchFoundHighScore = true;
                        }
                        else 
                        {
                            if (thirdHighScore.likesText.text == "1")
                            {
                                if (matchFound)
                                {
                                    GameEvents.current.FirstSecThirdMatch();
                                    matchFoundHighScore = true;
                                }
                            }
                            else
                            {
                                if (matchFound)
                                {
                                    GameEvents.current.FirstSecMatch();
                                    matchFoundHighScore = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (matchFound)
                        {
                            GameEvents.current.NoLikesMatch();
                            matchFoundHighScore = true;
                        }
                        else
                        {
                            if (thirdHighScore.likesText.text == "1")
                            {
                                if (matchFound)
                                {
                                    GameEvents.current.FirstThirdMatch();
                                    matchFoundHighScore = true;
                                }
                            }
                            else
                            {
                                if (matchFound)
                                {
                                    GameEvents.current.FirstSecMatch();
                                    matchFoundHighScore = true;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (secHighScore.likesText.text == "2")
                    {
                        if (matchFound)
                        {
                            GameEvents.current.FirstSecMatch();
                            matchFoundHighScore = true;
                        }
                        else
                        {
                            if (thirdHighScore.likesText.text == "2")
                            {
                                if (matchFound)
                                {
                                    GameEvents.current.FirstSecThirdMatch();
                                    matchFoundHighScore = true;
                                }
                            }
                            else
                            {
                                if (matchFound)
                                {
                                    GameEvents.current.FirstSecMatch();
                                    matchFoundHighScore = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (matchFound)
                        {
                            GameEvents.current.NoLikesMatch();
                            matchFoundHighScore = true;
                        }
                        else
                        {
                            if (thirdHighScore.likesText.text == "2")
                            {
                                if (matchFound)
                                {
                                    GameEvents.current.FirstSecMatch();
                                    matchFoundHighScore = true;
                                }
                            }
                            else
                            {
                                if (matchFound)
                                {
                                    GameEvents.current.FirstSecMatch();
                                    matchFoundHighScore = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        // DontMatch basically "resets" the game to the "Starting Point". It resets the revealed cards and the player can go on revealing cards again.
        public void DontMatch()
        {
            if (pressingAllowed)
            {
                moves --;
                movesLabel.text = "Moves: " + moves;
                _firstRevealed.Unreveal(); 
                _secondRevealed.Unreveal();
                if (_thirdRevealed != null)
                { 
                    _thirdRevealed.Unreveal();
                }
                matchFound = false;
                matchFoundHighScore = false;
                glow = false;
                revealingStopped = false;
                _firstRevealed = null;
                _secondRevealed = null;
                _thirdRevealed = null;            
                firstRevealed = false;
                secRevealed = false;
                thirdRevealed = false;
                keepChecking = true;
                pressingAllowed = false;
                doublePoly = false;
                shaking = true;
                GameEvents.current.BubblesDisabled();
                if (SceneManager.GetActiveScene().name == "Memory_LvL01")
                {
                    GameEvents.current.SaveHighScoreLvL1();
                }
                if (SceneManager.GetActiveScene().name == "Memory_LvL02")
                {
                    GameEvents.current.SaveHighScoreLvL2();
                }
                if (SceneManager.GetActiveScene().name == "Memory_LvL03")
                {
                    GameEvents.current.SaveHighScoreLvL3();
                }
            }
        }
        
        private void LittleHeartsUp()
        {
            littleHeart.SetActive(true);
        }

        private void MiddleHeartsUp()
        {
            middleHeart.SetActive(true);
        }

        private void LargeHeartsUp()
        {
            largeHeart.SetActive(true);
        }
    }
}
