﻿using TMPro;
using UnityEngine;

namespace Memory
{
    public class HighScoreSystem : MonoBehaviour
    {
        public int score;
        public TextMeshProUGUI scoreLabel;
        private bool noLikes;
        private bool firstSec;
        private bool firstThird;
        private bool allMatch;
        private void Start()
        {
            GameEvents.current.NoLikes += NoLikesHighScoreUp;
            GameEvents.current.FirstSec += FirstSecHighScoreUp;
            GameEvents.current.FirstThird += FirstThirdHighScoreUp;
            GameEvents.current.FirstSecThird += FirstSecThirdHighScoreUp;
            GameEvents.current.HighScoreUp += HighScoreUpOnClick;
            GameEvents.current.SaveScoreLvL1 += SaveHighScoreLvL1;
            GameEvents.current.SaveScoreLvL2 += SaveHighScoreLvL2;
            GameEvents.current.SaveScoreLvL3 += SaveHighScoreLvL3;
        }

        private void HighScoreUpOnClick()
        {
            if (noLikes)
            {
                score += 1;
                scoreLabel.text = "Score: " + score;
                GameEvents.current.LittleHeartsUp();
            }
            if (firstSec)
            {
                score += 3;
                scoreLabel.text = "Score: " + score;
                GameEvents.current.MiddleHeartsUp();
            }
            if (firstThird)
            {
                score += 3;
                scoreLabel.text = "Score: " + score;
                GameEvents.current.MiddleHeartsUp();
            }
            if (allMatch)
            {
                score += 5;
                scoreLabel.text = "Score: " + score;
                GameEvents.current.LargeHeartsUp();
            }

            noLikes = false;
            firstSec = false;
            firstThird = false;
            allMatch = false;
        }

        private void NoLikesHighScoreUp()
        {
            noLikes = true;
        }

        private void FirstSecHighScoreUp()
        {
            firstSec = true;
        }

        private void FirstThirdHighScoreUp()
        {
            firstThird = true;
        }

        private void FirstSecThirdHighScoreUp()
        {
            allMatch = true;
        }

        private void SaveHighScoreLvL1()
        {
            PlayerPrefs.SetInt("LvL1", score);
        }
        
        private void SaveHighScoreLvL2()
        {
            PlayerPrefs.SetInt("LvL2", score);
        }
        
        private void SaveHighScoreLvL3()
        {
            PlayerPrefs.SetInt("LvL3", score);
        }
    }
}
    
    
