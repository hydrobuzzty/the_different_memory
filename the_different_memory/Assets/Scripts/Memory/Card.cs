﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Creating a new Asset Menu option to quickly be able to make new ScriptableObjects
[CreateAssetMenu(fileName = "New Card", menuName = "Memory Card")]

// ScriptableObject class that sets declares different variables
public class Card : ScriptableObject
{
    public new string name;
    public string gender;
    public string sexuality;
    public string likes;
    
    public Sprite image;
}