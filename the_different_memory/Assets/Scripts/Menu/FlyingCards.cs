﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingCards : MonoBehaviour
{
   [SerializeField] 
   private GameObject blondGirl;
   
   [SerializeField] 
   private GameObject brownGirl;
   
   [SerializeField] 
   private GameObject greenGirl;
   
   [SerializeField] 
   private GameObject brownBoy;
   
   [SerializeField] 
   private GameObject greenBoy;

   void Start()
   {
      StartCoroutine(CardCoroutine());
   }
   
   IEnumerator CardCoroutine()
   {
      yield return new WaitForSeconds(0.1f);
      blondGirl.SetActive(true);
      
      yield return new WaitForSeconds(1f);
      brownBoy.SetActive(true);
      
      yield return new WaitForSeconds(1f);
      brownGirl.SetActive(true);
      
      yield return new WaitForSeconds(1f);
      greenBoy.SetActive(true);
      
      yield return new WaitForSeconds(1f);
      greenGirl.SetActive(true);
   }    
   

}
