﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace  CoATwoPR
{
    public class MainMenu : MonoBehaviour
    {
        public GameObject settingsOverlay;
        
        public GameObject creditsOverlay;
        
        
        //läd die Szene Intro
        public void playGame()
        {
            SceneManager.LoadScene("Intro");
        }
        
        public void showHub()
        {
            SceneManager.LoadScene("Hub");
        }
        //ein Canvas wird aktiviert
        public void loadingSettings()
        {
            settingsOverlay.SetActive(true);
        }
        
        public void loadingCredits()
        {
            creditsOverlay.SetActive(true);
        }
        
        public void unloadingCredits()
        {
            creditsOverlay.SetActive(false);
        }

        //der Canvas wird deaktiviert
        public void unloadingSettings()
        {
            settingsOverlay.SetActive(false);
        }

        //Beendet das Spiel
        public void QuitGame()
        {
            Debug.Log("Spiel wurde beendet!!");
            Application.Quit();
        }
    }
}