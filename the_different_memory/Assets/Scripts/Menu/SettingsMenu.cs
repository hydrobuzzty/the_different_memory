﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace CoATwoPR
{
    public class SettingsMenu : MonoBehaviour
    {
        public AudioMixer audioMixer;
        
        //Mit Zugriff über den Audiomixer auf den Mastermixer wird die Lautstärke eingestellt 
        public void SetVolume(float volume)
        {
            audioMixer.SetFloat("volume", volume);
        }

        //Die Qualität kann hier geändert werden die vorher in den project setting in inspector angepasst wurden
        public void SetQuality(int qualityIndex)
        {
            QualitySettings.SetQualityLevel(qualityIndex);
        }

        // mit einer checkbox kann man den fullscreen modus aktivieren 
        public void SetFullscreen(bool isFullscreen)
        {
            Screen.fullScreen = isFullscreen;
        }
    }
}