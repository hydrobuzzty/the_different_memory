﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableUI : MonoBehaviour
{
    [SerializeField]
    private GameObject slide01;
    
    [SerializeField]
    private GameObject slide02;
    
    [SerializeField]
    private GameObject slide03;
    
    public void disableSlide01()
    {
        slide01.SetActive(false);
    }
    
    public void disableSlide02()
    {
        slide02.SetActive(false);
    }
    
    public void disableSlide03()
    {
        slide03.SetActive(false);
    }
}
