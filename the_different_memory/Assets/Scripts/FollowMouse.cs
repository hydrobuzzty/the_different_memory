﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class FollowMouse : MonoBehaviour
{
    public float speed;
    

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 0;
        Vector3 objectpos = Camera.main.WorldToScreenPoint(transform.position);

        mousePos.x = mousePos.x - objectpos.x;
        mousePos.y = mousePos.y - objectpos.y;
        
        transform.rotation = Quaternion.Euler(new Vector3(0,0, 0));

        Vector3 targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        targetPos.z = 0;
        transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
    }
}
