﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlaylist : MonoBehaviour
{
    
    [SerializeField] 
    private AudioClip[] barAudio;

    private AudioSource audioSource;
    
    // Start is called before the first frame update
    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    AudioClip RandomClip()
    {
        int randomNumber = Random.Range(0, barAudio.Length);

        AudioClip randomClip = barAudio[randomNumber];

        return randomClip;
    }
    
    // Update is called once per frame
    void Update()
    {
        if (!audioSource.isPlaying)
        {
            audioSource.clip = RandomClip();

            audioSource.Play();
        }
        
    }
}
