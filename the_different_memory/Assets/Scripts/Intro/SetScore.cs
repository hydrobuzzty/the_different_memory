﻿using TMPro;
using UnityEngine;

namespace Intro
{
    public class SetScore : MonoBehaviour
    {
        public TextMeshProUGUI highScore1;
        public TextMeshProUGUI highScore2;
        public TextMeshProUGUI highScore3;
        private void Start()
        {
            highScore1.text = PlayerPrefs.GetInt("LvL1", 0).ToString();
            highScore2.text = PlayerPrefs.GetInt("LvL2", 0).ToString();
            highScore3.text = PlayerPrefs.GetInt("LvL3", 0).ToString();
        }
    }
}
